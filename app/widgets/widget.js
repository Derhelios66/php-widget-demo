class Widget extends HTMLElement {

    loading = false;

    constructor() {
        super();
        const template = document.getElementById("widget-template").content;
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.appendChild(template.cloneNode(true));

        if (this.getAttribute("async") !== null) {
            this.replaceContent();
        }
        if (this.getAttribute("refresh") !== null) {
            const refresh = +this.getAttribute("refresh");
            window.setInterval(() => {
                this.replaceContent()
            }, refresh * 1000);
        }
    }

    replaceContent() {
        if (this.loading) return;
        this.loading = true;

        const dirName = this.getAttribute("dirName");
        let content = null;
        for (const child of this.children) {
            if (child.getAttribute("slot") === "content") {
                content = child;
                break;
            }
        }

        content.innerHTML = "Lade";

        fetch(`/widgets/${ dirName }/render.php`).then(response => {
            return response.text();
        }).then(response => {
            content.innerHTML = response;
        }).finally(() => {
            this.loading = false;
        });
    }
}

window.customElements.define('s3-widget', Widget);
