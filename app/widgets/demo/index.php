<?php

require_once(realpath($_SERVER["DOCUMENT_ROOT"]) . "/widgets/widget.php");

$demoWidget = new Widget("demo", "Demo");
$demoWidget->async = true;
$demoWidget->refresh = 60;

$demoWidget->render();
