<?php

class Widget {
    public string $dirName;
    public string $title;
    public bool $async = false;
    public int|null $refresh = null;
    public function __construct(string $dirName, string $title) {
        $this->dirName = $dirName;
        $this->title = $title;
    }

    public function render() {
?>
    <s3-widget <?= $this->async ? "async" : "" ?> dirName="<?= $this->dirName ?>" <?= $this->refresh ? "refresh=\"".$this->refresh."\"" : "" ?> >
        <span slot="title"><?= htmlspecialchars($this->title) ?></span>
        <span slot="content">
            <?php
                if ($this->async) {
                    echo "Lade";
                } else {
                    require(realpath($_SERVER["DOCUMENT_ROOT"]) . "/widgets/". $this->dirName ."/render.php");
                }
            ?>
        </span>
    </s3-widget>
<?php
    }
}
